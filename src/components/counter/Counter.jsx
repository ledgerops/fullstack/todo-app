import React, { Component } from "react";
import './Counter.css'
import PropTypes from 'prop-types'

class Counter extends Component {


    constructor() {
        super();    //Error lot of developers make.
        //Define a javascript object
        this.state = {

            counter: 0
        }

        //Used arrow function to remove binding
        //this.increment = this.increment.bind(this);
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.reset = this.reset.bind(this);
    }


    render() {
        return (
            <div className="counter">
                <CounterButton by={1} increment={this.increment} decrement={this.decrement}></CounterButton>
                <CounterButton by={5} increment={this.increment} decrement={this.decrement}></CounterButton>
                <CounterButton by={10} increment={this.increment} decrement={this.decrement}></CounterButton>
                
                <div >
                    <button className="reset" onClick={this.reset}>Reset</button>
                </div>
                <span className="count">{this.state.counter}</span>
            </div>

        )
    }


    increment(by) { //Update the state
        console.log(`Increment from parent - ${by}`)
        // this.setState(
        //     {
        //         counter: this.state.counter
        //       }
        // );

        this.setState(
            (prevState) => {
                //counter: this.state.counter + 1          
                return {counter: prevState.counter + by}
            }
        );
    }


    decrement(by) { //Update the state
        console.log(`Decrement from parent - ${by}`)
        // this.setState(
        //     {
        //         counter: this.state.counter
        //       }
        // );

        this.setState(
            (prevState) => {
                //counter: this.state.counter + 1          
                return {counter: prevState.counter - by}
            }
        );
    }


    reset() { //Update the state
        console.log(`Reset from parent`)
        // this.setState(
        //     {
        //         counter: this.state.counter
        //       }
        // );

        this.setState(
            () => {
                //counter: this.state.counter + 1          
                return {counter: 0}
            }
        );
    }
}




class CounterButton extends Component {
    //Define the initial state in a constructor
    // State => counter = 0
    constructor() {
        super();    //Error lot of developers make.
        //Define a javascript object
        this.state = {

            counter: 0
        }

        //Used arrow function to remove binding
        //this.increment = this.increment.bind(this);
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
    }

    //If it is an arrow function the render is not required
    //render = () => {
    render() {
        //const countStyle = {fontSize : "100px"}

        return (
            <div className="counterComponent">

                {/* <button onClick={this.increment}>+1</button> */}

                <button onClick={this.increment}>+{this.props.by}</button>
                <button onClick={this.decrement}>-{this.props.by}</button>

                {/* <span className="count"
        style = {countStyle}
        >{this.state.counter}</span> */}
                {/* <span className="count">{this.state.counter}</span> */}
            </div>
        )
    }

    //If it is an arrow function  the render is not required anymore
    //increment = () => { //Update the state
    increment() { //Update the state

        //console.log('increment');

        //Very bad this to do.
        //this.state.counter++;

        //Best practice
        this.setState(
            {
                //counter: this.state.counter + 1          
                counter: this.state.counter + this.props.by
            }
        );

        this.props.increment(this.props.by)
    }


    decrement() { //Update the state

        this.setState(
            {
                counter: this.state.counter + this.props.by
            }
        );

        this.props.decrement(this.props.by)
    }
}

//Default values
CounterButton.defaultProps = {
    by: 100
}

//Type checking
CounterButton.propTypes = {
    by: PropTypes.number
}


export default Counter;