import React, {Component} from 'react'
import TodoDataService from '../../api/todo/TodoDataService';
import AuthenticationService from './AuthenticationService';
import moment from 'moment'

class ListTodosComponent extends Component {

    constructor(props){
        super(props)
        this.state = {
            todos : [
            //     {
            //     id: 1,
            //     description: 'Learn React',
            //     done: false,
            //     targetDate: new Date()
            // },
            // {
            //     id: 2,
            //     description: 'Learn Java',
            //     done: false,
            //     targetDate: new Date()
            // },
            // {
            //     id: 3,
            //     description: 'Learn Spring Boot',
            //     done: false,
            //     targetDate: new Date()
            // },
            // {
            //     id: 4,
            //     description: 'Learn Javascript',
            //     done: false,
            //     targetDate: new Date()
            // },
            // {
            //     id: 5,
            //     description: 'Learn Angular',
            //     done: false,
            //     targetDate: new Date()
            // }
        ],
        message: null
        }

        this.deleteTodoClicked = this.deleteTodoClicked.bind(this);
        this.refreshTodos = this.refreshTodos.bind(this);
        this.updateTodo = this.updateTodo.bind(this);
        this.addTodo = this.addTodo.bind(this);
    }



componentDidMount(){

    // let username = AuthenticationService.getLoggedInUserName();
    // TodoDataService.retrieveAllTodos(username)
    // .then((response) => {
    //     //console.log(response);
    //     this.setState({
    //         todos: response.data
    //     })
    // })
    this.refreshTodos();
}


refreshTodos(){
    let username = AuthenticationService.getLoggedInUserName();
    TodoDataService.retrieveAllTodos(username)
    .then((response) => {
        //console.log(response);
        this.setState({
            todos: response.data
        })
    })
}



deleteTodoClicked(id){
    let username = AuthenticationService.getLoggedInUserName();
    //console.log(id, username);

    //Call todo dataservice deleteTodo(username, id);
    TodoDataService.deleteTodo(username, id)
    .then((response) => {
            this.setState({
                message : `Delete of todo ${id} Successful`
            })
            this.refreshTodos();
    })
}

addTodo(){
    console.log("Add todo");
    this.props.history.push('/todos/-1')
}

updateTodo(id){
    console.log('Update todo '+id);
    this.props.history.push(`/todos/${id}`)
}

    render() {
        return (
            <div>
                <h1> List Todos </h1>
                {this.state.message && <div className="alert alert-success">{this.state.message}</div>}
                <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>description</th>
                            <th>is completed?</th>
                            <th>target date</th>
                            <th>Delete TODO</th>
                            <th>Update TODO</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.todos.map(
                                todo =>                             
                                <tr key={todo.id}>
                                    <td>{todo.id}</td>
                                    <td>{todo.description}</td>
                                    <td>{todo.done.toString()}</td>
                                    <td>{moment(todo.targetDate).format('YYYY-MM-DD')}</td>
                                    <td><button className="btn btn-warning" onClick={() => this.deleteTodoClicked(todo.id)}>Delete</button></td>
                                    <td><button className="btn btn-success" onClick={()=> this.updateTodo(todo.id)}>Update</button></td>
                                </tr>
                            )

                        }
                    </tbody>
                </table>
                <div className="row">
                    <button className="btn btn-success" onClick={this.addTodo}>Add</button>
                </div>
                </div>
                
            </div>
        )
    }
}


export default ListTodosComponent