import React, { Component } from "react";
import AuthenticatedService from './AuthenticationService'
import { Route, Redirect } from "react-router-dom";

class AuthenticatedRoute extends Component{

    render() {

        if(AuthenticatedService.isUserLoggedIn()){
            return <Route {...this.props}></Route>
        }else{
            return <Redirect to="/login"></Redirect>
        }
    }
}

export default AuthenticatedRoute;