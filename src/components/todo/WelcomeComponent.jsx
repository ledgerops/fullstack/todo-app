import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import HelloWorldService from '../../api/todo/HelloWorldService';

class WelcomeComponent extends Component{

    constructor(props){
        super(props)
        this.retrieveWelcomeMessage = this.retrieveWelcomeMessage.bind(this)
        this.state = {
            welcomeMessage:''
        }
        this.handleSuccessfulResponse = this.handleSuccessfulResponse.bind(this);
    }

    render() {
        return (
            <div>
                <h1>Welcome!</h1>
                <div className="container">
                    Welcome {this.props.match.params.name}. 
                    You can manage your todos <Link to="/todos">here</Link>
                </div>

                <div className="container">
                    Click here to get a welcome message.
                    <button onClick={this.retrieveWelcomeMessage} className="btn btn-success">Get welcome message</button>
                </div>

                <div className="container">
                    {this.state.welcomeMessage}
                </div>
            </div>
        )
    }

    retrieveWelcomeMessage(){
    
        console.log("Welcome Message");
        // HelloWorldService.executeHelloWorldService()
        // HelloWorldService.executeHelloWorldBeanService()
        HelloWorldService.executeHelloWorldPathService(this.props.match.params.name)
        .then((response) =>{
            console.log("Printing promise data -> ", response);
            this.handleSuccessfulResponse(response);

        })
        //.catch()
    }

    handleSuccessfulResponse(response){
        this.setState({
            welcomeMessage: response.data.message
        }) 
    }
}



export default WelcomeComponent;