import React, {Component} from 'react'
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom'
import AuthenticatedRoute from './AuthenticatedRoute';
import HeaderComponent from './HeaderComponent';
import ListTodosComponent from './ListTodosComponent';
import LoginComponent from './LoginComponent';
import WelcomeComponent from './WelcomeComponent';
import LogoutComponent from  './LogoutComponent';
import FooterComponent from './FooterComponent';
import TodoComponent from './TodoComponent';

class TodoApp extends Component{
    render() {
        return (
            <div className="todoApp">
                My Todo Application
                <Router>
                <HeaderComponent></HeaderComponent>
                    <Switch>
                        <Route path="/" exact component={LoginComponent}></Route>
                        <Route path="/login" component={LoginComponent}></Route>
                        <AuthenticatedRoute path="/todos/:id" component={TodoComponent}></AuthenticatedRoute>
                        <AuthenticatedRoute path="/welcome/:name" component={WelcomeComponent}></AuthenticatedRoute>
                        <AuthenticatedRoute path="/todos" component={ListTodosComponent}></AuthenticatedRoute>
                        <Route path="/logout" component={LogoutComponent}></Route>
                        <Route component={ErrorComponent}></Route>
                    </Switch>
                <FooterComponent></FooterComponent>
              </Router>
                {/* <LoginComponent>

                </LoginComponent>
                <WelcomeComponent/> */}
            </div>
        )
    }
}

function ErrorComponent() {
    return <div>An Error Occurred. I don't know what to do contact support.</div>
}

export default TodoApp