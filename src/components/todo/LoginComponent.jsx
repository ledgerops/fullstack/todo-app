import React, {Component} from 'react'
import AuthenticationService from './AuthenticationService'

class LoginComponent extends Component{

    //Best practice
    constructor(props){
        super(props)
        this.state = {
            username: 'kirito',
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.loginClicked = this.loginClicked.bind(this);
    }

    handleChange(event){
        console.log(event.target.name);
        //update username
        this.setState({
            [event.target.name]:event.target.value
        })
    }


    loginClicked(event){
        //kirito, password
        // if(this.state.username === 'kirito' && this.state.password === 'password'){
        //     //console.log("Success");
        //     AuthenticationService.registerSuccessfullLogin(this.state.username, this.state.password);
        //     this.setState({
        //         showSuccessMessage:true,
        //         hasLoginFailed: false
        //     })

        //     this.props.history.push(`/welcome/${this.state.username}`)
        // }else{
        //     //console.log('Failed');
        //     this.setState({
        //         showSuccessMessage:false,
        //         hasLoginFailed: true
        //     })
        // }

        // AuthenticationService
        // .executeBasicAuthenticationService(this.state.username, this.state.password)
        // .then(() =>{

        //     console.log("Within then block")
        //     AuthenticationService.registerSuccessfullLogin(this.state.username, this.state.password);
        //     this.setState({
        //         showSuccessMessage:true,
        //         hasLoginFailed: false
        //     })
        //     this.props.history.push(`/welcome/${this.state.username}`)
        // })
        // .catch(() => {
        //     this.setState({
        //         showSuccessMessage:false,
        //         hasLoginFailed: true
        //     })
        // })



        AuthenticationService
        .executeJwtAuthenticationService(this.state.username, this.state.password)
        .then((response) =>{

            console.log("Within then block")
            AuthenticationService.registerSuccessfullLoginForJwt(this.state.username, response.data.token);
            this.setState({
                showSuccessMessage:true,
                hasLoginFailed: false
            })
            this.props.history.push(`/welcome/${this.state.username}`)
        })
        .catch(() => {
            this.setState({
                showSuccessMessage:false,
                hasLoginFailed: true
            })
        })

        console.log(this.state)
    }

    render(){
        return (
            
            <div className="form">
                <h1>Login</h1>
                <div className="container"></div>

                {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid Credentials</div>}
                {/* <ShowInvalidCredentials hasLoginFailed={this.state.hasLoginFailed}></ShowInvalidCredentials> */}
                <ShowLoginSuccessMessage showSuccessMessage={this.state.showSuccessMessage}></ShowLoginSuccessMessage>
                User Name: <input type="text" name="username" value={this.state.username} onChange={this.handleChange}/>
                Password: <input type="password" name="password" value={this.state.password} onChange={this.handleChange}/> 
                <button className="btn btn-success" onClick={this.loginClicked}>Login</button>
            </div>
        )
    }


}

function ShowInvalidCredentials(props){
    console.log("Printing props"+props);
    if(props.hasLoginFailed){
        return <div>Invalid credentials</div>
    }
    return null
}


function ShowLoginSuccessMessage(props){
    console.log("Printing props"+props);
    if(props.showSuccessMessage){
        return <div>Success</div>
    }
    return null
}

export default LoginComponent