import axios from 'axios';
import {API_URL} from '../../components/todo/Constants';

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

axios.defaults.crossDomain = true;

class AuthenticationService {


    executeBasicAuthenticationService(username, password){

        let basicAuthHeader = 'Basic ' + window.btoa(username + ":" + password);
        return axios.get(`${API_URL}/basicauth`, {
            headers:{authorization:basicAuthHeader}
        })
    }



    executeJwtAuthenticationService(username, password){

        return axios.post(`${API_URL}/authenticate`,
        {
            username,
            password
        })
    }


    registerSuccessfullLogin(username, password){
        console.log('Register successfull');
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
        

        let basicAuthHeader = 'Basic ' + window.btoa(username + ":" + password);

        
        this.setupAxiousInterceptors(basicAuthHeader);


    }

    registerSuccessfullLoginForJwt(username,token){
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME,username)
        this.setupAxiousInterceptors(this.createJWTToken(token))
    }

    createJWTToken(token) {
        return 'Bearer ' + token
    }

    logout(){
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
    }

    isUserLoggedIn(){
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        if(user === null){
            console.log("Returning false");
            return false;
        }else{
            console.log("Returning true");
            return true;
        }
    }

    getLoggedInUserName(){
        let user = sessionStorage.getItem('authenticatedUser');
        if (user === null) return ''
        return user;
    }


    setupAxiousInterceptors(basicAuthHeader) {

        // let username = 'kirito'
        // let password = 'password'

        // let basicAuthHeader = 'Basic ' + window.btoa(username + ":" + password);

        axios.interceptors.request.use(
            (config) => {   

                if(this.isUserLoggedIn){
                    config.headers.Authorization = basicAuthHeader
                    
                }
                return config
            }
        )
    }
}

export default new AuthenticationService();