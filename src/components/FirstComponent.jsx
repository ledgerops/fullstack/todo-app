import React, { Component } from "react";

export default class FirstComponent extends Component{
    render(){
        return(
            <div className="firstComponent">
                Hello world
            </div>
        );
    }
}


//Module to be available to other modules
