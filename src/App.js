// import React from 'react';
// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;

/**
 * Old code. Now new version of react uses function component.
 */
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
 //import FirstComponent from './components/FirstComponent'
import FirstComponent from './components/FirstComponent'
import FifthComponent from './components/FifthComponent'
import SecondComponent from './components/SecondComponent'
import ThirdComponent from './components/ThirdComponent'
import CounterComponent from './components/counter/Counter'
import TodoApp from './components/todo/TodoApp'
import './bootstrap.css'
class App extends Component {
  render() {
    return (
      // <div className="App">
      //   <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <p>
      //       Edit <code>src/App.js</code> and save to reload.
      //   </p>
      //     <a
      //       className="App-link"
      //       href="https://reactjs.org"
      //       target="_blank"
      //       rel="noopener noreferrer"
      //     >
      //       Learn React
      //   </a>
      //   </header>
      // </div>
      <div className="App">

        {/* <CounterComponent by={1}></CounterComponent> */}
        {/* <CounterComponent ></CounterComponent>
        <CounterComponent by={5}></CounterComponent>
        <CounterComponent by={10}></CounterComponent> */}
        {/* <CounterComponent></CounterComponent> */}

        <TodoApp></TodoApp>
      </div>


    );
  }
}




class LearningComponent extends Component{
  render(){
    return(
      <div className="learningComponent">
        My Hello World
        <FirstComponent></FirstComponent>
        <SecondComponent></SecondComponent>
        <ThirdComponent></ThirdComponent>
        <FifthComponent></FifthComponent>
      </div>
    )
  }
}

export default App;
