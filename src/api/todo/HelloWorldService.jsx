import Axios from "axios";

class HelloWorldService {

    executeHelloWorldService(){

        //Return a promise back
        return Axios.get('http://localhost:8080/hello-world')
        //console.log('Execute service');
    }

    executeHelloWorldBeanService(){
        return Axios.get('http://localhost:8080/hello-world-bean');
    }


    executeHelloWorldPathService(name){

        // let username = 'kirito'
        // let password = 'password'

        //let basicAuthHeader = 'Basic ' + window.btoa(username + ":" + password)

         return Axios.get(`http://localhost:8080/hello-world/path-variable/${name}`)
         //, {
        //     headers : {
        //         authorization: basicAuthHeader
        //     }
        // }
    }
}

export default new HelloWorldService()