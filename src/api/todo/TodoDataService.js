import Axios from "axios";
import {API_URL} from '../../components/todo/Constants'

class TodoDataService {

    retrieveAllTodos(username){
        return Axios.get(`${API_URL}/users/${username}/todos`)
    }


    retrieveTodo(username, id){
        return Axios.get(`${API_URL}/${username}/todos/${id}`)
    }


    deleteTodo(username, id){
        return Axios.delete(`${API_URL}/${username}/todos/${id}`)
    }

    updateTodo(username, id, todo){
        return Axios.put(`${API_URL}/${username}/todos/${id}`,todo)
    }

}

export default new TodoDataService();